# DevopsCourseApp

# Installing the Default OpenJDK (Java 11) in ubuntu
sudo apt update

sudo apt install default-jdk

java -version

# Installing OpenJDK 8 #

Java 8 is still the most widely-used version of Java. Our application requires Java 8, you can install it by typing the following commands:

sudo apt update

sudo apt install openjdk-8-jdk


# Set the Default Java Version #

sudo update-alternatives --config java


