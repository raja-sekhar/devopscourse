package com.sample;

import com.sample.model.LiquorType;

import java.util.ArrayList;
import java.util.List;

public class LiquorService {

    public List getAvailableBrands(LiquorType type){

        List brands = new ArrayList();

        if(type.equals(LiquorType.DEVOPS)){
            brands.add("Jenkins");
            brands.add("Git");
            brands.add("maven");

        }else if(type.equals(LiquorType.CLOUD)){
            brands.add("AWS");
            brands.add("AZURE");

        }else if(type.equals(LiquorType.KUBERNETES)){
            brands.add("AKS");

        }else {
            brands.add("No Course Available");
        }
    return brands;
    }
}
